import { Checkbox, IconButton, ListItem, Typography } from "@material-ui/core";
import { CloseOutlined } from "@material-ui/icons";
import React from "react";

export default function Todo({ todo, toggleComplete, removeTodo }) {
  function handleCheckboxClick() {
    toggleComplete(todo.id);
  }

  function handleRemoveclick() {
    removeTodo(todo.id);
  }
  return (
    <ListItem style={{ display: "flex" }}>
      <Checkbox checked={todo.completed} onClick={handleCheckboxClick} />
      <Typography
        style={{
          textDecoration: todo.completed ? "line-through" : null,
          listStyleType: "none",
        }}
      >
        {todo.task}
      </Typography>
      <IconButton onClick={handleRemoveclick}>
        <CloseOutlined />
      </IconButton>
    </ListItem>
  );
}
